
import time
import os
import csv
import io
import channels.layers
from asgiref.sync import async_to_sync
from celery.decorators import task
from application_1.models import CsvFile
from django.core.files.base import ContentFile

@task(name="process_file")
def process_file(file_id):

	# procees and update db record with new file
	user_upload_file = CsvFile.objects.get(id=file_id)
	csv_file = user_upload_file.csv_file
	decoded_file = csv_file.read().decode('utf-8')
	io_string = io.StringIO(decoded_file)
	
	result_row = []
	reader = csv.DictReader(io_string)

	for row in reader:
		lat, lng = extract_lat_long_via_address(row['Address'])
		temp = {"Address":row['Address'], "Latitude": lat, "Longitude":lng}
		result_row.append(temp)

	csv_buffer = io.StringIO()
	csv_writer = csv.DictWriter(csv_buffer, fieldnames=["Address", "Latitude", "Longitude"])
	row = {"Address": "Address", "Latitude": "Latitude", "Longitude":"Longitude"}
	csv_writer.writerow(row)

	for each_row in result_row:
		csv_writer.writerow(each_row)

	csv_file = ContentFile(csv_buffer.getvalue().encode('utf-8'))
	user_upload_file.csv_file.save('output.csv', csv_file)
	user_upload_file.save()
	# end process and updation db with new file


	# initiate channel for send to user.
	channel_layer = channels.layers.get_channel_layer()

	async_to_sync(channel_layer.group_send)(
		"stream_excel",
        {
            'type': 'chat_message',
            'message': [{"db_id":file_id}]
        }
	)

import requests
GOOGLE_API_KEY = os.environ["GOOGLE_GEO_API_KEY"] 

def extract_lat_long_via_address(address_or_zipcode):
    lat, lng = None, None
    api_key = GOOGLE_API_KEY
    base_url = "https://maps.googleapis.com/maps/api/geocode/json"
    endpoint = f"{base_url}?address={address_or_zipcode}&key={api_key}"
    # see how our endpoint includes our API key? Yes this is yet another reason to restrict the key
    r = requests.get(endpoint)

    if r.status_code not in range(200, 299):
        return None, None
    try:
        results = r.json()['results'][0]
        lat = results['geometry']['location']['lat']
        lng = results['geometry']['location']['lng']
    except:
        pass
    return lat, lng	