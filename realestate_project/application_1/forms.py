from django import forms
from .models import CsvFile

class UploadForm(forms.ModelForm):
	class Meta:
		model = CsvFile
		fields = ('csv_file',)