import json
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer

class LetKnowUserDownload(WebsocketConsumer):
    def connect(self):
        self.stream_recieve = self.scope['url_route']['kwargs']['stream_recieve']
        self.stream_group_name = 'stream_%s' % self.stream_recieve
        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.stream_group_name,
            self.channel_name
        )

        self.accept()

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.stream_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        async_to_sync(self.channel_layer.group_send)(
            self.stream_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    # Receive message from room group
    def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message
        }))