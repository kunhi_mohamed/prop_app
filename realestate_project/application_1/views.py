from django.http import FileResponse
from django.shortcuts import render
from .forms import UploadForm
from .tasks import process_file
from .models import CsvFile

def index(request):
	context = {} 
	if request.POST: 
		form = UploadForm(request.POST, request.FILES) 
		if form.is_valid():
			save_file = form.save()
			process_file.delay(save_file.id)
	else: 
		form = UploadForm() 
	context['form'] = form
	return render(request, 'application_1/index.html', context)

def download(request, file_id):
	obj = CsvFile.objects.get(id=file_id)
	filename = obj.csv_file.path
	response = FileResponse(open(filename, 'rb'))
	return response	